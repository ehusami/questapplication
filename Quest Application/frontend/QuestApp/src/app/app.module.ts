import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BookingService } from './bookings/shared/booking.service'

import { AppComponent } from './app.component';
import { BookingsComponent } from './bookings/bookings.component';
import { BookComponent } from './bookings/book/book.component';
import { BookingListComponent } from './bookings/booking-list/booking-list.component';
import {HttpClientModule} from '@angular/common/http'
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    BookingsComponent,
    BookComponent,
    BookingListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [BookingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
