import { Component, OnInit } from '@angular/core';
import { BookingService } from '../shared/booking.service'
import { Booking, LUTimeSlot, LUTreatment } from '../shared/booking.model';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Http } from '@angular/http';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css'],
  providers: [BookingService]

})
export class BookComponent implements OnInit {
  errorMessage: string;
  public treatments1 = []

  booking: Booking;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  public treatments: LUTreatment[];
  public timeslot: LUTimeSlot[];

  constructor(private bookingService: BookingService, private toastr: ToastrService) { }


  ngOnInit() {
    // ****************************************GET BOOKING ***************************************//
    this.bookingService.GetAllBooking();

    // **************************************** RESET FORM  ***************************************//
    this.resetForm();

    // **************************************** GET TREATMENT ***************************************//
    this.bookingService.gettreatments().subscribe(data => {
      this.treatments1 = data, error => this.errorMessage = <any>error;
    });

    // **************************************** GET TIMESLOT  ***************************************//
    this.bookingService.gettimeslot().subscribe(res => {
      this.timeslot = res, error => this.errorMessage = <any>error;
    });
  }

  // **************************************** RESET FORM  ***************************************//
  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.bookingService.selectedBooking = {
      BookingId: null,
      Name: '',
      Email: '',
      Mobile: '',
      TreatmentId: 1,
      TimeSlotId: 1,
      Note: '',
      BookedDate: null,
      IsDeleted: false,
      TimeSlot: null,
      Treatment: null
    }
  }

  // **************************************** SAVE FORM IN DATABASE  ***************************************//
  OnSubmit(form: NgForm) {
    if (form.value.BookingId == null) {
      this.bookingService.PostBooking(form.value)
        .subscribe(data => {
          console.log(data)
          this.bookingService.GetAllBooking();
          this.resetForm(form);
          this.toastr.success('New Record Added Succcessfully', 'booked ');
        })
    }
    else {
      this.bookingService.PutBooking(form.value.BookingId, form.value)
        .subscribe(data => {
          this.resetForm(form);
          this.bookingService.GetAllBooking();
          this.toastr.info('Record Updated Successfully!', 'updated');
        });
    }

  }

}
