import { Injectable } from '@angular/core';
import { Booking, LUTimeSlot, LUTreatment } from './booking.model';
import { HttpClient } from '@angular/common/http'
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class BookingService {

  selectedBooking: Booking;
  bookingList: Booking[];
  timeSlotList: LUTimeSlot[];
  treatmentList: LUTreatment[];

  constructor(private http: Http, private http1: HttpClient) { }

  // **************************************** GET TREATMENTS (OBSERVABLE)  ***************************************//
  gettreatments(): Observable<LUTreatment[]> {
    return this.http1.get<LUTreatment[]>('http://esraaappservice.azurewebsites.net/api/Bookings/treatment');
  }

  // **************************************** GET TIMSLOT (OBSERVABLE) ***************************************//
  gettimeslot(): Observable<LUTimeSlot[]> {
    return this.http.get('http://esraaappservice.azurewebsites.net/api/Bookings/timeslot')
      .map((data: Response) => {
        return data.json();
      })
  }

  // **************************************** GET BOOKING (OBSERVABLE) ***************************************//
  getbooking(): Observable<Booking[]> {
    return this.http.get('http://esraaappservice.azurewebsites.net/api/Bookings')
      .map((data: Response) => {
        return data.json();
      })
  }

  // **************************************** SAVE FUNCTION  ***************************************//
  PostBooking(book: Booking) {

    var body = JSON.stringify(book);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.post('http://esraaappservice.azurewebsites.net/api/Bookings', body, requestOptions).map(x => x);
  }

  // **************************************** GET BOOKING(PROMISE)  ***************************************//
  GetAllBooking() {
    return this.http.get('http://esraaappservice.azurewebsites.net/api/Bookings')
      .map((data: Response) => {
        console.log(data.json())
        return data.json() as Booking[];
      }).subscribe(x => {
        this.bookingList = x;
      })
  }

  // **************************************** GET TIMESLOT(PROMISE)  ***************************************//
  GetTimeSlot() {
    return this.http.get('http://esraaappservice.azurewebsites.net/api/Bookings/timeslot')
      .map((data: Response) => {
        console.log(data.json())
        return data.json() as LUTimeSlot[];
      }).toPromise().then(x => {
        this.timeSlotList = x;
      })
  }

  // **************************************** GET TREATMENTS (PROMISE) ***************************************//
  GetTreatment() {
    return this.http.get('http://esraaappservice.azurewebsites.net/api/Bookings/treatment')
      .map((data: Response) => {
        console.log(data.json())
        return data.json() as LUTreatment[];
      }).toPromise().then(x => {
        this.treatmentList = x;
      })
  }

  // **************************************** UPDATE FUNCTION ***************************************//
  PutBooking(id: number, book: Booking) {
    var body = JSON.stringify(book);
    console.log("teeeeeeeeeeeeeeeeeeeest", body);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.post('http://esraaappservice.azurewebsites.net/api/Bookings/' + id,
      body,
      requestOptions).map(res => res);
  }

  // **************************************** DELETE FUNCTION ***************************************//
  DeleteBooking(id) {
    // var body = JSON.stringify(book);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return this.http.post('http://esraaappservice.azurewebsites.net/api/Bookings/delete/' + id, requestOptions).map(res => res);
  }

}
