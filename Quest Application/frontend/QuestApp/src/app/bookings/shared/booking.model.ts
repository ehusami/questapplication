export interface Booking {
    BookingId: number;
    Name: string;
    Email: string;
    Mobile: string;
    TreatmentId: number;
    TimeSlotId: number;
    Note: string;
    BookedDate: Date;
    IsDeleted: Boolean;
    TimeSlot:LUTimeSlot;
    Treatment:LUTreatment
}

export interface LUTimeSlot {
    ID: number;
    TimeSlotTitle: number;
}

export interface LUTreatment {
    ID: number;
    TreatmentTitle: string;
    TreatmentDescription: string;
}
