﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Quest1.Models;

namespace Quest1.Pages.test
{
    public class IndexModel : PageModel
    {
        private readonly Quest1.Models.QuestContext _context;

       
        public IndexModel(Quest1.Models.QuestContext context)
        {
            _context = context;
        }
        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }

        public IList<Booking> Booking { get; set; }
        public IList<LUTimeSlot> LUTimeSlots { get; set; }
        public IList<LUTreatment> LUTreatments { get; set; }


        public async Task OnGetAsync(string sortOrder,
            string currentFilter, string searchString)
        {
            CurrentSort = sortOrder;
            NameSort = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            DateSort = sortOrder == "Date" ? "date_desc" : "Date";

            CurrentFilter = searchString;

            IQueryable<Booking> List = from s in _context.Booking
                                       where s.IsDeleted.Equals(false)
                                       select s;

            if (!String.IsNullOrEmpty(searchString))
            {
                List = List.Where(s => s.Name.Contains(searchString)
                                       || s.Mobile.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    List = List.OrderByDescending(s => s.Name);
                    break;
                case "Date":
                    List = List.OrderBy(s => s.BookedDate);
                    break;
                case "date_desc":
                    List = List.OrderByDescending(s => s.BookedDate);
                    break;
                default:
                    List = List.OrderBy(s => s.Name);
                    break;
            }


            Booking = await List.Include(c=>c.LUTimeSlot).Include(c=>c.LUTreatment).AsNoTracking().ToListAsync();

        }
    }
}
