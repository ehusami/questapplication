﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Quest1.Models;

namespace Quest1.Pages.test
{
    public class DeleteModel : PageModel
    {
        private readonly Quest1.Models.QuestContext _context;

        public DeleteModel(Quest1.Models.QuestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Booking Booking { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Booking = await _context.Booking
                .Include(b => b.LUTimeSlot)
                .Include(b => b.LUTreatment).SingleOrDefaultAsync(m => m.BookingId == id);

            if (Booking == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Booking = await _context.Booking.FindAsync(id);

            if (Booking != null)
            {
                //_context.Booking.Remove(Booking);
                //await _context.SaveChangesAsync();


                using (_context)
                {
                    var result = _context.Booking.SingleOrDefault(b => b.BookingId == id);
                    if (result != null)
                    {
                        result.IsDeleted = true;
                        _context.SaveChanges();
                    }
                }
            }

            return RedirectToPage("./Index");
        }
    }
}
