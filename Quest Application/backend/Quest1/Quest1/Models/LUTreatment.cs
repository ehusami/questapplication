﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Models
{
    public class LUTreatment
    {
        [Key]
        public int ID { get; set; }
        public string TreatmentTitle { get; set; }
        public string TreatmentDescription { get; set; }

        public ICollection<Booking> Booking { get; set; }
    }
}
