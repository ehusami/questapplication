﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quest1.Data.Interfaces;
using Quest1.Models;

namespace Quest1.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]

    [Produces("application/json")]
    [Route("api/Bookings")]
    public class BookingsController : Controller
    {
        private readonly QuestContext _context;
        private readonly IBookingRepository _bookingRepository;
        private readonly ILUTimeSlotRepository _timeSlotRepository;
        private readonly ILUTreatmentRepository _treatmentRepository ;


        public BookingsController( IBookingRepository bookingRepository, ILUTimeSlotRepository timeSlotRepository, ILUTreatmentRepository treatmentRepository, QuestContext context)
        {
        
            _bookingRepository = bookingRepository;
            _timeSlotRepository = timeSlotRepository;
            _treatmentRepository = treatmentRepository;
            _context = context;
        }

        // GET: api/Bookings
        [HttpGet]
        public IEnumerable<Booking> GetBooking()
        {
            var book= _bookingRepository.GetAllBookings();
            foreach (var obj in book)
            {
                obj.LUTreatment = _context.LUTreatment.Single(e => e.ID == obj.TreatmentId);
                obj.LUTimeSlot = _context.LUTimeSlot.Single(e => e.ID == obj.TimeSlotId);

            }
            return book;
        }
        [HttpGet("treatment")]
        public IEnumerable<LUTreatment> GetTreatment()
        {
            return _treatmentRepository.GetTreatments();
        }
        [HttpGet("timeslot")]
        public IEnumerable<LUTimeSlot> GetTimeSlot()
        {
            return _timeSlotRepository.GetTimeSlot();
        }
        // GET: api/Bookings/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBooking([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var booking = _bookingRepository.GetBookingsById(id);

            if (booking == null)
            {
                return NotFound();
            }

            return Ok(booking);
        }

        // PUT: api/Bookings/5
        [HttpPost("{id}")]
        public async Task<IActionResult> PutBooking([FromRoute] int id, [FromBody] Booking booking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != booking.BookingId)
            {
                return BadRequest();
            }
            
            try
            {
                _bookingRepository.UpdateBooking(id, booking);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(booking);
        }

        // POST: api/Bookings
        [HttpPost]
        public async Task<IActionResult> PostBooking([FromBody] Booking booking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _bookingRepository.AddBooking(booking);

            return CreatedAtAction("GetBooking", new { id = booking.BookingId }, booking);
        }

        // DELETE: api/Bookings/5
        [HttpPost("delete/{id}")]
        public async Task<IActionResult> DeleteBooking([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _bookingRepository.DeleteBooking(id);

            return Ok();
        }

        private bool BookingExists(int id)
        {
            return _bookingRepository.BookingExists(id);
        }
    }
}