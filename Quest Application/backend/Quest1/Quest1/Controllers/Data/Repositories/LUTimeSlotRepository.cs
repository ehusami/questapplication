﻿using Quest1.Data.Interfaces;
using Quest1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Data.Repositories
{
    public class LUTimeSlotRepository : ILUTimeSlotRepository
    {
        private readonly QuestContext _questContext;
        public LUTimeSlotRepository (QuestContext questContext)
        {
            _questContext = questContext;
        }

        public IEnumerable<LUTimeSlot> GetTimeSlot()
        {
            IList<LUTimeSlot> timeSlots = new List<LUTimeSlot>();
            var list = from s in _questContext.LUTimeSlot
                       select s;

            foreach (LUTimeSlot time in list)
            {
                timeSlots.Add(time);
            }
            return timeSlots;
        }
    }
}
