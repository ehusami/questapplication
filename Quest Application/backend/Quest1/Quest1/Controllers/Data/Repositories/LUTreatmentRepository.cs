﻿using Quest1.Data.Interfaces;
using Quest1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Data.Repositories
{
    public class LUTreatmentRepository : ILUTreatmentRepository
    {
        private readonly QuestContext _questContext;
        public LUTreatmentRepository (QuestContext questContext)
        {
            _questContext = questContext;
        }

        public IEnumerable<LUTreatment> GetTreatments()
        {
            IList<LUTreatment> treatments = new List<LUTreatment>();
            var list = from s in _questContext.LUTreatment
                       select s;

            foreach (LUTreatment tretment in list)
            {
                treatments.Add(tretment);
            }
            return treatments;

        }
    }
}
