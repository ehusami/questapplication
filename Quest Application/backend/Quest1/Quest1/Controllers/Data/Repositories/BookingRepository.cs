﻿using Microsoft.EntityFrameworkCore;
using Quest1.Data.Interfaces;
using Quest1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Data.Repositories
{
    public class BookingRepository : IBookingRepository
    {
        private readonly QuestContext _questContext;
        public BookingRepository(QuestContext questContext)
        {
            _questContext = questContext;
        }

        public  void AddBooking(Booking booking)
        {
            _questContext.Booking.Add(booking);
            _questContext.SaveChangesAsync();
        }

        public bool BookingExists(int id)
        {
            return _questContext.Booking.Any(e => e.BookingId == id);
        }

        public void DeleteBooking(int id)
        {
            var Booking = GetBookingsById(id);
            Booking.IsDeleted = true;
            _questContext.SaveChanges();
        }

        public IEnumerable<Booking> GetAllBookings()
        {
            IList<Booking> bookings = new List<Booking>();
            var list = from s in _questContext.Booking
                       where s.IsDeleted.Equals(false) 
                       && s.TimeSlotId.Equals(s.LUTimeSlot.ID)
                       && s.TreatmentId.Equals(s.LUTreatment.ID)
                       select s;
            
            foreach (Booking book in list)
            {
                bookings.Add(book);
            }
            return bookings;
        }


        public Booking GetBookingsById(int id)
        {
            return _questContext.Booking.FirstOrDefault(m => m.BookingId == id);
        }

        public void UpdateBooking(int id, Booking bookings)
        {
            Booking booking = GetBookingsById(id);
            booking.BookedDate = bookings.BookedDate;
            booking.Note = bookings.Note;
            booking.TimeSlotId = bookings.TimeSlotId;
            booking.TreatmentId = bookings.TreatmentId;
            _questContext.Update(booking);
            _questContext.SaveChanges();
        }
    }
}
