﻿using Quest1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Data.ViewModels
{
    public class BookingViewModel
    {
        public List<Booking> Bookings { get; set; }

    }
}
