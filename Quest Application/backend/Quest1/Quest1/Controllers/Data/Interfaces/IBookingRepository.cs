﻿using Quest1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Data.Interfaces
{
    public interface IBookingRepository
    {
        IEnumerable<Booking> GetAllBookings();
        Booking GetBookingsById(int id);
        void AddBooking(Booking booking);
        void DeleteBooking(int id);
        void UpdateBooking(int id,Booking booking);
        bool BookingExists(int id);
    }
}
